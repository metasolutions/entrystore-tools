/*
 * Copyright (c) 2007-2017 MetaSolutions AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.entrystore.tools;

import org.openrdf.model.BNode;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * @author Hannes Ebner
 */
public class Replacers {

	/**
	 * Replaces matching namespaces in a repository.
	 */
	public static void replaceNamespace(Repository oldRepo, Repository newRepo, String oldNamespace, String newNamespace, boolean verbose) {
		replaceResources(oldRepo, newRepo, oldNamespace, newNamespace, false, verbose);
	}

	/**
	 * Replaces matching URIs in a repository.
	 */
	public static void replaceUri(Repository oldRepo, Repository newRepo, String oldUri, String newUri, boolean verbose) {
		replaceResources(oldRepo, newRepo, oldUri, newUri, true, verbose);
	}

	/**
	 * Replaces matching namespaces or URIs in a repository.
	 *
	 * @param oldRepo
	 * @param newRepo
	 * @param oldNamespace
	 * @param newNamespace
	 * @param exactMatch If true resource.equals(oldNamespace) is used, if false resource.startsWith(oldNamespace) is used.
	 * @param verbose
	 */
	private static void replaceResources(Repository oldRepo, Repository newRepo, String oldNamespace, String newNamespace, boolean exactMatch, boolean verbose) {
		if (oldNamespace.equals(newNamespace)) {
			out("Old and new namespace are the same, aborting");
		}
		RepositoryConnection rcRead = null;
		RepositoryConnection rcWrite = null;
		RepositoryResult<Statement> oldStmnts = null;
		try {
			long changeCount = 0;
			long progressCount = 0;
			Set<Long> printedProgress = new HashSet();
			rcRead = oldRepo.getConnection();
			rcWrite = newRepo.getConnection();
			rcWrite.begin(); // TODO: after upgrade to Sesame >= 2.8 check which isolation level performs the best
			long totalTripleCount = rcRead.size();
			out("Found " + totalTripleCount + " statements in repository");
			ValueFactory vf = rcRead.getValueFactory();
			oldStmnts = rcRead.getStatements(null, null, null, false);
			while (oldStmnts.hasNext()) {
				Statement oldStmnt = oldStmnts.next();
				boolean changed = false;
				Resource s = oldStmnt.getSubject();
				org.openrdf.model.URI p = oldStmnt.getPredicate();
				Value o = oldStmnt.getObject();
				Resource c = oldStmnt.getContext();
				if (s instanceof org.openrdf.model.URI) {
					String sStr = s.stringValue();
					if (!exactMatch) {
						if (sStr.startsWith(oldNamespace)) {
							s = vf.createURI(replaceString(s.stringValue(), oldNamespace, newNamespace));
							changed = true;
						}
					} else {
						if (sStr.equals(oldNamespace)) {
							s = vf.createURI(newNamespace);
							changed = true;
						}
					}
				}
				if (p instanceof org.openrdf.model.URI) {
					String pStr = p.stringValue();
					if (!exactMatch) {
						if (pStr.startsWith(oldNamespace)) {
							p = vf.createURI(replaceString(p.stringValue(), oldNamespace, newNamespace));
							changed = true;
						}
					} else {
						if (pStr.equals(oldNamespace)) {
							p = vf.createURI(newNamespace);
							changed = true;
						}
					}
				}
				if (o instanceof org.openrdf.model.URI) {
					String oStr = o.stringValue();
					if (!exactMatch) {
						if (oStr.startsWith(oldNamespace)) {
							o = vf.createURI(replaceString(o.stringValue(), oldNamespace, newNamespace));
							changed = true;
						}
					} else {
						if (oStr.equals(oldNamespace)) {
							o = vf.createURI(newNamespace);
							changed = true;
						}
					}
				}
				if (c != null && c instanceof org.openrdf.model.URI) {
					String cStr = c.stringValue();
					if (!exactMatch) {
						if (cStr.startsWith(oldNamespace)) {
							c = vf.createURI(replaceString(c.stringValue(), oldNamespace, newNamespace));
							changed = true;
						}
					} else {
						if (cStr.equals(oldNamespace)) {
							c = vf.createURI(newNamespace);
							changed = true;
						}
					}
				}

				if (changed) {
					Statement newStmnt = vf.createStatement(s, p, o, c);
					if (verbose) {
						out("Changed " + oldStmnt + " to " + newStmnt);
					}
					rcWrite.add(newStmnt, newStmnt.getContext());
					changeCount++;
				} else {
					rcWrite.add(oldStmnt, oldStmnt.getContext());
					if (verbose) {
						out("Copied " + oldStmnt + " without modification");
					}
				}

				progressCount++;
				if (totalTripleCount > 5000 && progressCount > 0) {
					long percent = Math.round(((double) progressCount / (double) totalTripleCount) * 100);
					if ((percent % 10) == 0) {
						if (!printedProgress.contains(percent)) {
							if (percent < 100) {
								outsl(percent + "%...");
							} else {
								out(percent + "%");
							}
							printedProgress.add(percent);
						}
					}
				}
			}
			rcWrite.commit();
			out("Wrote " + changeCount + " statements with new namespace to new repository at " + newRepo.getDataDir());
		} catch (RepositoryException re) {
			try {
				rcWrite.rollback();
			} catch (RepositoryException e) {
				out(e.getMessage());
			}
			out(re.getMessage());
		} finally {
			if (oldStmnts != null) {
				try {
					oldStmnts.close();
				} catch (RepositoryException e) {
					out(e.getMessage());
				}
			}
			if (rcRead != null) {
				try {
					rcRead.close();
				} catch (RepositoryException e) {
					out(e.getMessage());
				}
			}
			if (rcWrite != null) {
				try {
					rcWrite.close();
				} catch (RepositoryException e) {
					out(e.getMessage());
				}
			}
		}
	}

	private static String replaceString(String data, String from, String to) {
		StringBuffer buf = new StringBuffer(data.length());
		int pos = -1;
		int i = 0;
		while ((pos = data.indexOf(from, i)) != -1) {
			buf.append(data.substring(i, pos)).append(to);
			i = pos + from.length();
		}
		buf.append(data.substring(i));
		return buf.toString();
	}

	/**
	 * Replaces BNodes, with new ones. Works per context and with the repository's value factory.
	 * Useful if something has gone wrong with BNodes that have to be repaired.
	 *
	 * FIXME needs rewrite to use new repo because of lack of transaction isolation
	 */
	public static void rewriteBNodes(Repository oldRepo, Repository newRepo, boolean verbose) {
		RepositoryConnection rcRead = null;
		RepositoryConnection rcWrite = null;
		RepositoryResult<Resource> ngs = null;
		try {
			long changeCount = 0;
			long progressCount = 0;
			Set<Long> printedProgress = new HashSet();
			rcRead = oldRepo.getConnection();
			long totalTripleCount = rcRead.size();
			out("Found " + totalTripleCount + " statements in repository");
			rcWrite = newRepo.getConnection();
			rcWrite.begin();
			ValueFactory vf = rcRead.getValueFactory();
			ngs = rcRead.getContextIDs();
			while (ngs.hasNext()) {
				Resource ng = ngs.next();
				Map<BNode, BNode> old2newBNode = new HashMap();
				RepositoryResult<Statement> oldStmnts = rcRead.getStatements(null, null, null, false, ng);
				try {
					while (oldStmnts.hasNext()) {
						Statement oldStmnt = oldStmnts.next();
						boolean replace = false;
						Resource subj = oldStmnt.getSubject();
						URI pred = oldStmnt.getPredicate();
						Value obj = oldStmnt.getObject();
						if (subj instanceof BNode) {
							subj = getNewBNode(old2newBNode, (BNode) subj, vf);
							replace = true;
						}
						if (obj instanceof BNode) {
							obj = getNewBNode(old2newBNode, (BNode) obj, vf);
							replace = true;
						}
						if (replace) {
							Statement newStmnt = vf.createStatement(subj, pred, obj, ng);
							if (verbose) {
								out("Changed " + oldStmnt + " to " + newStmnt);
							}
							rcWrite.add(newStmnt, ng);
						} else {
							rcWrite.add(oldStmnt, ng);
							if (verbose) {
								out("Copied " + oldStmnt + " without modification");
							}

						}

						progressCount++;
						if (totalTripleCount > 5000 && progressCount > 0) {
							long percent = Math.round(((double) progressCount / (double) totalTripleCount) * 100);
							if ((percent % 10) == 0) {
								if (!printedProgress.contains(percent)) {
									if (percent < 100) {
										outsl(percent + "%...");
									} else {
										out(percent + "%");
									}
									printedProgress.add(percent);
								}
							}
						}
					}
				} finally {
					oldStmnts.close();
				}
			}
			rcWrite.commit();
			out("Wrote " + changeCount + " statements with new namespace to new repository at " + newRepo.getDataDir());
		} catch (RepositoryException re) {
			try {
				rcWrite.rollback();
			} catch (RepositoryException e) {
				out(e.getMessage());
			}
			out(re.getMessage());
		} finally {
			if (ngs != null) {
				try {
					ngs.close();
				} catch (RepositoryException e) {
					out(e.getMessage());
				}
			}
			if (rcRead != null) {
				try {
					rcRead.close();
				} catch (RepositoryException e) {
					out(e.getMessage());
				}
			}
			if (rcWrite != null) {
				try {
					rcWrite.close();
				} catch (RepositoryException e) {
					out(e.getMessage());
				}
			}
		}
	}

	public static void copy(Repository oldRepo, Repository newRepo) {
		RepositoryConnection rcRead = null;
		RepositoryConnection rcWrite = null;
		RepositoryResult<Statement> oldStmnts = null;
		try {
			long progressCount = 0;
			Set<Long> printedProgress = new HashSet();
			rcRead = oldRepo.getConnection();
			rcWrite = newRepo.getConnection();
			rcWrite.begin(); // TODO: after upgrade to Sesame >= 2.8 check which isolation level performs the best
			long totalTripleCount = rcRead.size();
			out("Found " + totalTripleCount + " statements in source repository");
			ValueFactory vf = rcRead.getValueFactory();
			oldStmnts = rcRead.getStatements(null, null, null, false);
			while (oldStmnts.hasNext()) {
				rcWrite.add(oldStmnts.next());
				progressCount++;
				if (totalTripleCount > 5000 && progressCount > 0) {
					long percent = Math.round(((double) progressCount / (double) totalTripleCount) * 100);
					if ((percent % 10) == 0) {
						if (!printedProgress.contains(percent)) {
							if (percent < 100) {
								outsl(percent + "%...");
							} else {
								out(percent + "%");
							}
							printedProgress.add(percent);
						}
					}
				}
			}
			rcWrite.commit();
			out("Wrote " + progressCount + " statements with new namespace to new repository at " + newRepo.getDataDir());
		} catch (RepositoryException re) {
			try {
				rcWrite.rollback();
			} catch (RepositoryException e) {
				out(e.getMessage());
			}
			out(re.getMessage());
		} finally {
			if (oldStmnts != null) {
				try {
					oldStmnts.close();
				} catch (RepositoryException e) {
					out(e.getMessage());
				}
			}
			if (rcRead != null) {
				try {
					rcRead.close();
				} catch (RepositoryException e) {
					out(e.getMessage());
				}
			}
			if (rcWrite != null) {
				try {
					rcWrite.close();
				} catch (RepositoryException e) {
					out(e.getMessage());
				}
			}
		}
	}

	private static BNode getNewBNode(Map<BNode, BNode> m, BNode oldBNode, ValueFactory vf) {
		BNode newBNode = m.get(oldBNode);
		if (newBNode == null) {
			newBNode = vf.createBNode();
			m.put(oldBNode, newBNode);
		}
		return newBNode;
	}

	private static void out(String s) {
		System.out.println(s);
	}

	/**
	 * Output on same line.
	 * @param s String to print.
	 */
	private static void outsl(String s) {
		System.out.print(s);
	}

}