/*
 * Copyright (c) 2007-2017 MetaSolutions AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.entrystore.tools;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.util.RDFInserter;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;
import org.openrdf.rio.trig.TriGWriter;
import org.openrdf.rio.turtle.TurtleWriter;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.Date;

/**
 * @author Hannes Ebner
 */
public class Repositories {

	public static void exportToFile(Repository repository, String file, boolean verbose) {
		RepositoryConnection rc = null;
		OutputStream out = null;
		Date before = new Date();
		if ("-".equals(file)) {
			verbose = false;
		}
		if (verbose) {
			out("Exporting repository to " + file);
		}
		long tripleCount = 0;
		long contextCount = 0;
		long timeDiff = 0;
		try {
			rc = repository.getConnection();
			if ("-".equals(file)) {
				out = new BufferedOutputStream(System.out);
			} else {
				out = new BufferedOutputStream(new FileOutputStream(file));
			}
			RDFWriter writer = new TriGWriter(out);
			rc.export(writer);
			timeDiff = new Date().getTime() - before.getTime();
			if (verbose) {
				tripleCount = rc.size();
				RepositoryResult<Resource> contextResult = rc.getContextIDs();
				for (; contextResult.hasNext(); contextResult.next()) {
					contextCount++;
				}
			}
		} catch (Exception e) {
			out(e.getMessage());
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException ioe) {
					out(ioe.getMessage());
				}
			}
			if (rc != null) {
				try {
					rc.close();
				} catch (RepositoryException re) {
					out(re.getMessage());
				}
			}
		}
		if (verbose) {
			out("Export of " + tripleCount + " triples in " + contextCount + " contexts finished in " + timeDiff + " ms");
		}
	}

	public static void importFromFile(Repository repository, String file, boolean verbose) {
		RepositoryConnection rc = null;
		InputStream in = null;
		if ("-".equals(file)) {
			verbose = false;
		}
		try {
			Date before = new Date();
			if (verbose) {
				out("Importing repository from " + file);
			}

			rc = repository.getConnection();
			rc.begin();
			final ValueFactory vf = rc.getValueFactory();
			if ("-".equals(file)) {
				in = new BufferedInputStream(System.in);
			} else {
				in = new BufferedInputStream(new FileInputStream(file));
			}
			RDFFormat format = RDFFormat.forFileName(file);
			RDFParser parser = Rio.createParser(format);
			parser.setDatatypeHandling(RDFParser.DatatypeHandling.IGNORE);
			parser.setRDFHandler(new RDFInserter(rc));
			parser.parse(in, "");
			rc.commit();

			if (verbose) {
				long timeDiff = new Date().getTime() - before.getTime();
				long tripleCount = rc.size();
				long countextCount = 0;
				RepositoryResult<Resource> contextResult = rc.getContextIDs();
				for (; contextResult.hasNext(); contextResult.next()) {
					countextCount++;
				}
				out("Import of " + tripleCount + " triples in " + countextCount + " contexts finished in " + timeDiff + " ms");
			}
		} catch (Exception e) {
			try {
				rc.rollback();
			} catch (RepositoryException e1) {
				out(e1.getMessage());
			}
			out(e.getMessage());
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException ioe) {
					out(ioe.getMessage());
				}
			}
			if (rc != null) {
				try {
					rc.close();
				} catch (RepositoryException re) {
					out(re.getMessage());
				}
			}
		}
	}

	public static String getContext(Repository repository, String contextUri, boolean verbose) {
		RepositoryConnection rc = null;
		RepositoryResult<Statement> rr = null;
		try {
			rc = repository.getConnection();
			rr = rc.getStatements(null, null, null, false, rc.getValueFactory().createURI(contextUri));
			if (!rr.hasNext()) {
				return "No triples found for context " + contextUri;
			}
			StringWriter sb = new StringWriter();
			RDFWriter writer = new TurtleWriter(sb);
			writer.startRDF();
			while (rr.hasNext()) {
				writer.handleStatement(rr.next());
			}
			writer.endRDF();
			return sb.toString();
		} catch (Exception e) {
			out(e.getMessage());
		} finally {
			if (rr != null) {
				try {
					rr.close();
				} catch (RepositoryException e) {
					out(e.getMessage());
				}
			}
			if (rc != null) {
				try {
					rc.close();
				} catch (RepositoryException re) {
					out(re.getMessage());
				}
			}
		}
		return null;
	}

	private static void out(String s) {
		System.out.println(s);
	}

}
