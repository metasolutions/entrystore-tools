/*
 * Copyright (c) 2007-2017 MetaSolutions AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.entrystore.tools;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.nativerdf.NativeStore;

import java.io.File;
import java.util.Arrays;

/**
 * EntryStore tools to manipulate Sesame repositories.
 *
 * @author Hannes Ebner
 *
 * TODO switch to new parameters library for more flexible parsing; with that the path of the new repo can
 * be made an optional parameter: if provided the modified triples are written to a new repo at a specified
 * path, if omitted a default path is generated like now
 */
public class EntryStoreTools {

	private static boolean verbose = false;

	public static void main(String[] args) {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.WARN);

		if (args.length > 2 && "--verbose".equalsIgnoreCase(args[args.length-1])) {
			verbose = true;
			args = Arrays.copyOf(args, args.length-1);
		}

		if (args.length > 1) {
			String command = args[0];
			String repositoryPath = args[1];
			if (!new File(repositoryPath).isDirectory()) {
				out("Repository path must be a directory: " + repositoryPath);
				return;
			}

			// we need this later without slash at the end, to construct a new repo path
			if (repositoryPath.endsWith("/")) {
				repositoryPath = repositoryPath.substring(0, repositoryPath.length() - 1);
			}

			NativeStore store = new NativeStore(new File(repositoryPath));
			Repository repository = new SailRepository(store);
			NativeStore newStore = new NativeStore(new File(repositoryPath + ".modified"));
			Repository newRepo = new SailRepository(newStore);

			try {
				repository.initialize();
				if ("import".equalsIgnoreCase(command)) {
					if (args.length == 3) {
						Repositories.importFromFile(repository, args[2], verbose);
					} else {
						out("Wrong parameter count");
					}
				} else if ("export".equalsIgnoreCase(command)) {
					if (args.length == 3) {
						Repositories.exportToFile(repository, args[2], verbose);
					} else {
						out("Wrong parameter count");
					}
				} else if ("copy".equalsIgnoreCase(command)) {
					if (args.length == 3) {
						newStore = new NativeStore(new File(args[2]));
						newRepo = new SailRepository(newStore);
						newRepo.initialize();
						Replacers.copy(repository, newRepo);
					} else {
						out("Wrong parameter count");
					}
				} else if ("replace-namespace".equalsIgnoreCase(command)) {
					if (args.length == 4) {
						newRepo.initialize();
						Replacers.replaceNamespace(repository, newRepo, args[2], args[3], verbose);
					} else {
						out("Wrong parameter count");
					}
				} else if ("replace-bnodes".equalsIgnoreCase(command)) {
					if (args.length == 2) {
						newRepo.initialize();
						Replacers.rewriteBNodes(repository, newRepo, verbose);
					} else {
						out("Wrong parameter count");
					}
				} else if ("replace-uri".equalsIgnoreCase(command)) {
					if (args.length == 4) {
						newRepo.initialize();
						Replacers.replaceUri(repository, newRepo, args[2], args[3], verbose);
					} else {
						out("Wrong parameter count");
					}
				} else if ("count-triples".equalsIgnoreCase(command)) {
					if (args.length == 2) {
						out(Long.toString(Statistics.countTriples(repository)));
					}
				} else if ("count-contexts".equalsIgnoreCase(command)) {
					if (args.length == 2) {
						out(Long.toString(Statistics.countContexts(repository)));
					}
				} else if ("show-context".equalsIgnoreCase(command)) {
					if (args.length == 3) {
						out(Repositories.getContext(repository, args[2], verbose));
					}
				} else {
					out("Unknown command: " + command);
				}
			} catch (RepositoryException re) {
				out("Unable to load repository: " + re.getMessage());
			} finally {
				try {
					repository.shutDown();
				} catch (RepositoryException re) {
					out("Could not close repository: " + re.getMessage());
				}
				if (newRepo.isInitialized()) {
					try {
						newRepo.shutDown();
					} catch (RepositoryException re) {
						out("Could not close repository: " + re.getMessage());
					}
				}
			}
		} else {
			out("EntryStore tools -- http://bitbucket.org/metasolutions/entrystore-tools");
			out("");
			out("Usage: es-tools <command> <repository path> <parameters...> [--verbose]");
			out("");
			out("Commands:");
			out("");
			out("  export <repository path> <RDF file (TriG) or - for stdout> [--verbose]");
			out("  import <repository path> <RDF file (any format) or - for stdin> [--verbose]");
			out("  copy <source repository> <destination repository>");
			out("  replace-namespace <repository path> <old namespace> <new namespace> [--verbose]");
			out("  replace-uri <repository path> <old URI> <new URI> [--verbose]");
			out("  replace-bnodes <repository path> [--verbose]");
			out("  count-triples <repository path>");
			out("  count-contexts <repository path>");
			out("  show-context <repository path> <context URI>");
			out("");
		}
	}

	private static void out(String s) {
		System.out.println(s);
	}

}