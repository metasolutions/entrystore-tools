# EntryStore tool chain

The EntryStore tool chain is a collection of low level tools to manipulate a Sesame repository.
The tools are intended to be used with an EntryStore backend, but can be used with any Sesame repository.

Currently only Sesame Native Store is supported. Additional backends will be added on demand.

## Installation

By running `mvn install` all source files are compiled and assembled together with their dependencies into the `target/dist` directory.

## Execution

After setting the executable flag with `chmod +x` the EntryStore tools can be run out of the bin-directory.

Calling the executable without parameter shows a list of available commands and their parameters.

## Usage

`est <command> <repository path> <parameters...> [--verbose]`

### Commands and parameters

  * `copy <source repository> <destination repository>`
  * `count-contexts <repository path>`
  * `count-triples <repository path>`
  * `export <repository path> <RDF file (TriG) or - for stdout>`
  * `import <repository path> <RDF file (any format) or - for stdin>`
  * `replace-bnodes <repository path>`
  * `replace-namespace <repository path> <old namespace> <new namespace>`
  * `replace-uri <repository path> <old URI> <new URI>`
  * `show-context <repository path> <context URI>`

### Usage with Docker

An up-to-date Docker image is available on [Docker Hub](https://hub.docker.com/r/metasolutions/entrystore-tools/). It is necessary to provide the repos/files to operate on as volumes to the Docker container.

#### Examples

```
docker run --rm -v ./entrystore-data:/workdir -it metasolutions/entrystore-tools est count-triples /workdir/store
docker run --rm -v ./entrystore-data:/workdir -it metasolutions/entrystore-tools est replace-namespace /workdir/store 'http://localhost/store/' 'https://some.domain.tld/store/' --verbose
docker run --rm -v ./entrystore-data:/workdir -it metasolutions/entrystore-tools est import /workdir/newstore /workdir/dump.rdf
```

## Backlog

  * Improved command line parameter handling using a CLI library
  * Support for any RDF formats that are supported by Sesame/RDF4J
